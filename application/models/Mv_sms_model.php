<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Mv_sms_model
 *
 * @author Akula
 */
class Mv_sms_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getSmsPhoneNumbers($limit = 10) {
        $table_name = "mv_sms_campaign";
        $id = array();
        $columns = "phonenumber";
        $filters = array('sent' => NULL);
        
        return parent::getResources($table_name, $id, $columns, $filters, FALSE, FALSE, $limit);
    }
    
    function updateSmsCampaign($phone, $update_param) {
        $this->db->where('phonenumber', $phone);
        return $this->db->update('mv_sms_campaign',$update_param);
    }
    
    function queueSms($phonenumber, $text_message) {
        return $this->db->insert(
                        'mv_sms_queue', array(
                    'phonenumber' => $phonenumber,
                    'text_message' => $text_message,
                    'date_add' => date('Y-m-d')
                        )
        );
    }

}

?>
