<?php

//error_reporting(E_ALL);

defined('BASEPATH') OR exit('No direct script access allowed');

class Newpaybill extends Admin_Controller {

    private $service_id;
    private $originator_conversation_id;
    private $sp_password;
    private $password;
    private $sp_id;
    private $registerUrlLink;
    private $validation_url = "https://10.176.0.119:443/mpesa_api/admin/newpaybill/validatetestrequest";
    private $confirmation_url = "https://10.176.0.119:443/mpesa_api/admin/newpaybill/confirmtestpayment";
    private $shortcode;
    private $timestamp;
    private $is_secure_link = false;
    private $is_live = true;
//    private $cainfo = "./mpesa_api/mpesa_api_ssl/intermediateCA.cer";
//    private $sslcert = "./mpesa_api/mpesa_api_ssl/TestBroker.cer";
//    private $sslkey = "./mpesa_api/mpesa_api_ssl/privateKey.key";
    private $customer_notification_numbers = array('254713348090');
    private $policyNumber;
    private $policyNo;

    function __construct() {

        parent::__construct();

        if ($this->is_live) {
            $this->sp_id = "100725";
            $this->service_id = "100725000";
            $this->password = "Password01";
            $this->shortcode = "328100";
            $this->registerUrlLink = "https://portal.safaricom.com/registerURL";
            $this->validation_url = "https://10.176.0.119:443/mpesa_api/admin/newpaybill/validaterequest";
            $this->confirmation_url = "https://10.176.0.119:443/mpesa_api/admin/newpaybill/confirmpayment";
        } else {
            $this->sp_id = "100725";
            $this->service_id = "100725000";
            $this->password = "Password01";
            $this->shortcode = "328100";
            $this->registerUrlLink = "https://portal.safaricom.com/tregisterURL";
            $this->validation_url = "https://10.176.0.118:443/mpesa_api/admin/newpaybill/validatetestrequest";
            $this->confirmation_url = "https://10.176.0.118:443/mpesa_api/admin/newpaybill/confirmtestpayment";
        }

        $this->originator_conversation_id = "Jubinsure-" . time();
//        $this->timestamp = date("Y-m-d H:i:s", time());
        $this->timestamp = date("YmdHis", time());

        $hashData = $this->sp_id . $this->password . $this->timestamp;
        $spPasswordHash = hash('sha256', $hashData);
        //$this->sp_password = base64_encode($spPasswordHash);
        $this->load->library('CI_infobip');
        $this->load->model('Mv_paybill_model');
    }


    public function registerUrl() {

        $xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:req=\"https://api-v1.gen.mm.vodafone.com/mminterface/request\">" .
            "<soapenv:Header>" .
            "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\">" .
            "<tns:spId>{$this->sp_id}</tns:spId>" .
            "<tns:spPassword>{$this->password}</tns:spPassword>" .
            "<tns:timeStamp>{$this->timestamp}</tns:timeStamp>" .
            "<tns:serviceId>{$this->service_id}</tns:serviceId>" .
            "</tns:RequestSOAPHeader>" .
            "</soapenv:Header>" .
            "<soapenv:Body>" .
            "<req:RequestMsg><![CDATA[<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
            "<request xmlns=\"http://api-v1.gen.mm.vodafone.com/mminterface/request\">" .
            "<Transaction>" .
            "<CommandID>RegisterURL</CommandID>" .
            "<OriginatorConversationID>{$this->originator_conversation_id}</OriginatorConversationID>" .
            "<Parameters>" .
            "<Parameter>" .
            "<Key>ResponseType</Key>" .
            "<Value>Completed</Value>" .
            "</Parameter>" .
            "</Parameters>" .
            "<ReferenceData>" .
            "<ReferenceItem>" .
            "<Key>ValidationURL</Key>" .
            "<Value>{$this->validation_url}</Value>" .
            "</ReferenceItem>" .
            "<ReferenceItem>" .
            "<Key>ConfirmationURL</Key>" .
            "<Value>{$this->confirmation_url}</Value>" .
            "</ReferenceItem>" .
            "</ReferenceData>" .
            "</Transaction>" .
            "<Identity>" .
            "<Caller>" .
            "<CallerType>0</CallerType>" .
            "<ThirdPartyID/>" .
            "<Password/>" .
            "<CheckSum/>" .
            "<ResultURL/>" .
            "</Caller>" .
            "<Initiator>" .
            "<IdentifierType>1</IdentifierType>" .
            "<Identifier/>" .
            "<SecurityCredential/>" .
            "<ShortCode/>" .
            "</Initiator>" .
            "<PrimaryParty>" .
            "<IdentifierType>1</IdentifierType>" .
            "<Identifier/>" .
            "<ShortCode>{$this->shortcode}</ShortCode>" .
            "</PrimaryParty>" .
            "</Identity>" .
            "<KeyOwner>1</KeyOwner>" .
            "</request>]]></req:RequestMsg>" .
            "</soapenv:Body>" .
            "</soapenv:Envelope>";

// get completed xml document

        $dom = new DOMDocument;
        $dom->preserveWhiteSpace = FALSE;
        $dom->loadXML("$xml");
        $dom->formatOutput = TRUE;


        echo '<pre>';
        print_r(htmlentities($dom->saveXml()));
        echo '</pre>';

        $response = $this->sendTransactionRequest($xml);

        $dome = new DOMDocument;
        $dome->preserveWhiteSpace = FALSE;
        $dome->loadXML(($response));
        $dome->formatOutput = TRUE;

        echo '<pre>';
        print_r(htmlentities($dome->saveXml()));
        echo '</pre>';

        exit();
    }

    private function getRequestHeaders($contentLength) {
        return array(
            'Content-type: text/xml;charset="utf-8"',
            'Accept: text/xml',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
            'Content-length: ' . $contentLength
        );
    }

    private function sendTransactionRequest($request) {

        $request = trim(preg_replace('/\s+/', ' ', $request));
        $contentLength = strlen($request);
        $headers = self::getRequestHeaders($contentLength);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $this->registerUrlLink);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_POST, 1); //POST

        curl_setopt($ch, CURLOPT_TIMEOUT, 120);

        if (!$this->is_secure_link) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        }

        if ($this->is_secure_link) {
            curl_setopt($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

            curl_setopt($ch, CURLOPT_CAINFO, $this->cainfo);
            curl_setopt($ch, CURLOPT_SSLCERT, $this->sslcert);
            curl_setopt($ch, CURLOPT_SSLKEY, $this->sslkey);
        }

        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $response = curl_exec($ch);

        if ($response === false) {
            echo 'Curl error: ' . curl_error($ch);
        } else {
            echo 'Operation completed without any errors';
        }

        curl_close($ch);

        return $response;
    }

    public function index()
    {
        $this->verify_login();
        $crud = $this->generate_crud('transactions');
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'business_shortcode', 'bill_reference_number', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Bill Reference Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('customer_id','Customer Full Name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $this->mPageTitle = 'Transactions';
        $this->render_crud();
    }

    public function customers(){
        $this->verify_login();
        $crud = $this->generate_crud('customers');
        $crud->columns('firstname', 'middlename', 'lastname', 'phonenumber');
        $this->unset_crud_fields('email');
        $crud->display_as('firstname','First Name')
            ->display_as('middlename','Middle Name')
            ->display_as('lastname','Last Name');
        $crud->unset_add();
        $this->mPageTitle = 'Customers';
        $this->render_crud();
    }

    public function customerstransaction(){
        $this->verify_login();
        $crud = $this->generate_crud('customer_transactions');
        $crud->columns('customer_id', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->set_relation('transaction_id', 'transactions', '{transaction_id} {transaction_amount}');
        $this->unset_crud_fields('email');
        $crud->display_as('firstname','First Name')
            ->display_as('middlename','Middle Name')
            ->display_as('lastname','Last Name')
            ->display_as('transaction_id','Transaction ID')
            ->display_as('customer_id','Customer Name');
        $crud->unset_add();
        $this->mPageTitle = 'Customer\'s Transaction';
        $this->render_crud();
    }

    public function generalMedicalTransactions(){
        $this->verify_login();
        $crud = $this->generate_crud('transactions');
        $crud->where('business_shortcode', 328102);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'business_shortcode', 'bill_reference_number', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Pay Bill Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('customer_id','Customer Full Name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $this->mPageTitle = 'General and Medical Business Transactions';
        $this->render_crud();
    }

    public function lifeTransactions(){
        $this->verify_login();
        $crud = $this->generate_crud('transactions');
        $crud->where('business_shortcode', 328100);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'business_shortcode', 'bill_reference_number', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Pay Bill Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('customer_id','Customer Full Name');
        $crud->unset_add();
        $crud->unset_delete();
        $this->mPageTitle = 'Life Business Transactions';
        $this->render_crud();
    }

    public function pensionTransactions(){
        $this->verify_login();
        $crud = $this->generate_crud('transactions');
        $crud->where('business_shortcode', 328103);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'business_shortcode', 'bill_reference_number', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Pay Bill Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('customer_id','Customer Full Name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $this->mPageTitle = 'Pension Business Transactions';
        $this->render_crud();
    }

    public function customer_transaction(){
        $this->verify_login();
        $crud = $this->generate_crud('customer_transactions');
        $crud->columns('customer_id', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->set_relation('transaction_id','transactions','{transaction_id}');
        //$crud->set_relation('transaction_id','transactions','{business_shortcode}');
        $crud->display_as('customer_id','Customer Name')
            ->display_as('transaction_id','Transaction Name');
        $crud->unset_add();
        $crud->unset_delete();
        $crud->unset_edit();
        $this->mPageTitle = 'Customer Transaction';
        $this->render_crud();
    }

    //Life Receipting
    public function lifeReceipt(){
        $this->verify_login();
        $crud = $this->generate_crud('receipt');
        $crud->where('business_shortcode', 328100);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'withdrawal','business_shortcode', 'bill_reference_number', 'agent_broker_code', 'agent_name', 'agent_number', 'policy_owner', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Policy Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('withdrawal','Withdrawn')
            ->display_as('customer_id','Payer Name')
            ->display_as('policy_owner','Policy Owner Name')
            ->display_as('agent_name','Agent/Broker Name')
            ->display_as('agent_broker_code','Agent/Broker Code')
            ->display_as('agent_number','Agent/Broker Number');
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $this->mPageTitle = 'Life Department Receipts';
        $this->render_crud();
    }

    //Pension Receipting
    public function pensionReceipt(){
        $this->verify_login();
        $crud = $this->generate_crud('receipt');
        $crud->where('business_shortcode', 328103);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'withdrawal','business_shortcode', 'bill_reference_number', 'agent_broker_code', 'agent_name', 'agent_number', 'policy_owner', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Policy Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('withdrawal','Withdrawn')
            ->display_as('customer_id','Payer Name')
            ->display_as('policy_owner','Policy Owner Name')
            ->display_as('agent_name','Agent/Broker Name')
            ->display_as('agent_broker_code','Agent/Broker Code')
            ->display_as('agent_number','Agent/Broker Number');
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $this->mPageTitle = 'Pension Department Receipts';
        $this->render_crud();
    }

    //General and Medical Receipting
    public function generalMedicalReceipt(){
        $this->verify_login();
        $crud = $this->generate_crud('receipt');
        $crud->where('business_shortcode', 328102);
        $crud->columns('transaction_type', 'transaction_id', 'transaction_amount', 'withdrawal','business_shortcode', 'bill_reference_number', 'agent_broker_code', 'agent_name', 'agent_number', 'policy_owner', 'customer_id');
        $this->unset_crud_fields('organisation_account_balance', 'transaction_id');
        $crud->set_relation('customer_id','customers','{firstname} {middlename} {lastname}');
        $crud->display_as('business_shortcode','Business Shortcode')
            ->display_as('bill_reference_number','Policy Number')
            ->display_as('transaction_amount','Transaction Amount')
            ->display_as('phonenumber','Phone Number')
            ->display_as('withdrawal','Withdrawn')
            ->display_as('customer_id','Payer Name')
            ->display_as('policy_owner','Policy Owner Name')
            ->display_as('agent_name','Agent/Broker Name')
            ->display_as('agent_broker_code','Agent/Broker Code')
            ->display_as('agent_number','Agent/Broker Number');
        $crud->unset_add();
        $crud->unset_edit();
        $crud->unset_delete();
        $this->mPageTitle = 'General and Medical Department Receipts';
        $this->render_crud();
    }
    /*
     * Receive validation request from broker and process it
     */
    public function validateTestRequest() {

        $data = json_decode(file_get_contents('php://input'), true);
        print_r($data);
//        echo $data["operacion"];

//        $postdata_results = file_get_contents("php://input");
//        $postdata = isset($postdata_results) ? $postdata_results : '';
//        if (!is_dir('./successful_validate_payments')) {
//            mkdir('./successful_validate_payments', 0777, true);
//        }
//        $name = "./successful_validate_payments/log-" . time() . ".xml";
//        $f = fopen("$name", "w");
//        fwrite($f, $postdata);
//        fclose($f);
//
//        $transaction_details = array(
//            'TransType' => NULL,
//            'TransID' => NULL,
//            'TransTime' => NULL,
//            'TransAmount' => NULL,
//            'BusinessShortCode' => NULL,
//            'BillRefNumber' => NULL,
//            'MSISDN' => NULL,
//            'FirstName' => NULL,
//            'LastName' => NULL,
//            'MiddleName' => NULL
//        );
//        $response = $this->validatePaymentResponse();
////        header("Content-type : text/xml");
//        echo $response;
    }

    public function validateRequest() {

        $postdata_results = file_get_contents("php://input");
        $postdata = isset($postdata_results) ? $postdata_results : '';
        if (!is_dir('./successful_validate_payments')) {
            mkdir('./successful_validate_payments', 0777, true);
        }
        $name = "./successful_validate_payments/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $postdata);
        fclose($f);

        $transaction_details = array(
            'TransType' => NULL,
            'TransID' => NULL,
            'TransTime' => NULL,
            'TransAmount' => NULL,
            'BusinessShortCode' => NULL,
            'BillRefNumber' => NULL,
            'MSISDN' => NULL,
            'FirstName' => NULL,
            'LastName' => NULL,
            'MiddleName' => NULL
        );

        /*$xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $postdata);
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        if ($responseArray) {
            $transaction_details['TransType'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransType'] ? : FALSE;
            $transaction_details['TransID'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransID'] ? : FALSE;
            $transaction_details['TransTime'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransTime'] ? : FALSE;
            $transaction_details['TransAmount'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransAmount'] ? : FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['BusinessShortCode'] ? : FALSE;
            $transaction_details['BillRefNumber'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['BillRefNumber'] ? : FALSE;
            $transaction_details['MSISDN'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['MSISDN'] ? : FALSE;

            foreach ($responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['KYCInfo'] as $key => $kycinfo_array) {
                foreach ($kycinfo_array as $kycinfo_key => $kycinfo) {
                    $new_kycinfo[] = $kycinfo;
                }
            }

            if (sizeof($new_kycinfo) == 4) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
            }

            if (sizeof($new_kycinfo) == 6) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['MiddleName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
            }
        }*/

        $response = $this->validatePaymentResponse();
        header("Content-type : text/xml");
        echo $response;
    }

    /*
     * send validation response to broker
     */

    public function validatePaymentResponse($third_party_transaction_id = "1234560000088888") {
        $validate_response = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c2b=\"http://cps.huawei.com/cpsinterface/c2bpayment\">"
            . "<soapenv:Header/>"
            . "<soapenv:Body>"
            . "<c2b:C2BPaymentValidationResult>"
            . "<ResultCode>0</ResultCode>"
            . "<ResultDesc>Service processing successful</ResultDesc>"
            . "<ThirdPartyTransID>$third_party_transaction_id</ThirdPartyTransID>"
            . "</c2b:C2BPaymentValidationResult>"
            . "</soapenv:Body>"
            . "</soapenv:Envelope>";

        return $validate_response;
    }

    public function packageSavePaymentConfirmation($postdata) {

        $transaction_details = array(
            'TransactionType' => NULL,
            'TransID' => NULL,
            'TransTime' => NULL,
            'TransAmount' => NULL,
            'BusinessShortCode' => NULL,
            'BillRefNumber' => NULL,
            'MSISDN' => NULL,
            'FirstName' => NULL,
            'LastName' => NULL,
            'MiddleName' => NULL
        );

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $postdata);
        $xml = simplexml_load_string($xml);

        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        if ($responseArray) {
            $transaction_details['TransType'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransType'] ?: FALSE;
            $transaction_details['TransID'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransID'] ?: FALSE;
            $transaction_details['TransTime'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransTime'] ?: FALSE;
            $transaction_details['TransAmount'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransAmount'] ?: FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BusinessShortCode'] ?: FALSE;
            $transaction_details['BillRefNumber'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BillRefNumber'] ?: FALSE;
            $transaction_details['OrgAccountBalance'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['OrgAccountBalance'] ?: FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BusinessShortCode'] ?: FALSE;
            $transaction_details['MSISDN'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['MSISDN'] ?: FALSE;
            foreach ($responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['KYCInfo'] as $key => $kycinfo_array) {
                foreach ($kycinfo_array as $kycinfo_key => $kycinfo) {
                    $new_kycinfo[] = $kycinfo;
                }
            }

            if (sizeof($new_kycinfo) == 4) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
            }

            if (sizeof($new_kycinfo) == 6) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['MiddleName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[5]) ? $new_kycinfo[5] : FALSE;
            }

            $TransType = $transaction_details['TransType'];
            $TransID = $transaction_details['TransID'];
            $TransTime = $transaction_details['TransTime'];
            $TransAmount = $transaction_details['TransAmount'];
            $BusinessShortCode = $transaction_details['BusinessShortCode'];
            $BillRefNumber = $transaction_details['BillRefNumber'];
            $OrgAccountBalance = $transaction_details['OrgAccountBalance'];
            $MSISDN = $transaction_details['MSISDN'];
            $FirstName = $transaction_details['FirstName'];
            $MiddleName = $transaction_details['MiddleName'];
            $LastName = $transaction_details['LastName'];
            $sender_ip_address = $this->input->ip_address();



            /*-- Implementation of the Policy For Life validation--*/
            $policyNo = '$policyNo';
            $vehicleRegNo = '$vehicleRegNo';
            $BillRefNumber = strtoupper($BillRefNumber);
            if($BusinessShortCode == 328100) {
                $data = array(
                    query => "query ($policyNo: String!){customerDetails (policyNo: $policyNo) { edges { node   {  policyNo customerName email phoneNumber agentNo agentCode agentName } }}}",
                    variables => "{\"policyNo\":\"$BillRefNumber\"}"
                );

                $data_string = json_encode($data);

                //$curl = curl_init('http://192.168.50.27:5000/graphql');
                $curl = curl_init('http://192.168.8.174:5000/graphql');
            }
            else if($BusinessShortCode == 328102){
                $data = array(
                    query => "query ($vehicleRegNo: String!){customerDetails (vehicleRegNo: $vehicleRegNo) { edges { node   {  vehicleRegNo customerName email phoneNumber agentCode agentName } }}}",
                    variables => "{\"vehicleRegNo\":\"$BillRefNumber\"}"
                );

                $data_string = json_encode($data);

                //$curl = curl_init('http://192.168.50.27:5001/graphql');
                $curl = curl_init('http://192.168.8.174:5001/graphql');
            }
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");

            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );

            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);  // Make it so the data coming back is put into a string
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);  // Insert the data

            // Send the request
            $result = curl_exec($curl);

            // Free up the resources $curl is using
            curl_close($curl);

            //echo $result;

            $result = json_decode($result, true);
            $node = $result['data']['customerDetails']['edges'][0];
            $id = "";
            $customer_policynumber = "";
            $customer_name = "";
            $customer_email = "";
            $customer_dbphonenumber = "";
            $agent_number = "";
            $agent_code = "";
            $agent_name = "";
            foreach ($node as $key => $value) {
                $id = $value['id'];
                $customer_policynumber = $value['policyNo'];
                $customer_name = $value['customerName'];
                $customer_email = $value['email'];
                $customer_dbphonenumber = $value['phoneNumber'];
                $agent_number = $value['agentNo'];
                $agent_code = $value['agentCode'];
                $agent_name = $value['agentName'];
            }

            if ($customer_dbphonenumber != "") {
                $truncatedCustomerPhoneNumber = substr($customer_dbphonenumber, 1);
                $customer_phonenumber = '254' . $truncatedCustomerPhoneNumber;
            } else{
                $customer_phonenumber = "";
            }

            //send text message
            $sms_amount = round($TransAmount);
            $payer_name = ucfirst(strtolower($FirstName));
            //$payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. An acknowledgment receipt has been sent to your email address. Thank you for choosing Jubilee Insurance.";
            $payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. For an acknowledgment receipt to be sent to your email address. Click on this link https://jubileeinsure.com/email_form/ to register your email address with Jubilee Insurance. Thank you for choosing Jubilee Insurance.";
            // $this->ci_infobip->sendsms($payer_message, $MSISDN);

            //Random number genration
            $today = date('YmdHi');
            $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
            $range = $today - $startDate;
            $rand = rand(0, $range);
            $receiptRandomNumber = $startDate + $rand;

            //Generate Receipt
            $data = [
                "customerFullName" => $customer_name,
                "transferredAmount" => $TransAmount,
                "transactionType" => $TransType,
                "businessShortCode" => $BusinessShortCode,
                "billReferenceNumber" => $BillRefNumber,
                "mobileNumber" => $MSISDN,
                "transactionID" => $TransID,
                "generalMedicalReceiptRandonNumber" => $receiptRandomNumber,
            ];

            //load the view and save it into $html variable
            $html=$this->load->view('email/receipt', $data, true);

            if (!is_dir('./receipts_successful_payments')) {
                mkdir('./receipts_successful_payments', 0777, true);
            }
            //this the the PDF filename that user will get to download

            $pdfFilePath = "./receipts_successful_payments/".$FirstName."_jubilee_insurance_receipt-".time().".pdf";

            $pdfFileURL = base_url()."receipts_successful_payments/".$FirstName."_jubilee_insurance_receipt-".time().".pdf";

            //load mPDF library
            $this->load->library('m_pdf');

            //generate the PDF from the given html
            $this->m_pdf->pdf->WriteHTML($html);

            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "F");

            //TODO::Check if customer phone number contains numbers


            /*life policies validation*/

            //Yes policy, Yes Email, Yes Phonenumber
            if($BusinessShortCode == 328100 && $customer_policynumber != "" && $customer_email != "" && $customer_phonenumber != ""){
                $payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($payer_message, $MSISDN);
                $customer_message = "Hi $customer_name, This is to confirm that the payment of KES. $sms_amount for your policy $BillRefNumber has been successfully completed. An acknowledgment receipt has been sent to your email address. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($customer_message, $customer_phonenumber);
                $this->sendMail($customer_name, $customer_email, $pdfFilePath);
                //$this->Mv_paybill_model->insertCustomerSMS($MSISDN,$payer_message,$customer_message,$customer_name,$customer_phonenumber,$BillRefNumber);
            } //Yes policy, No Email, Yes Phonenumber
            else if($BusinessShortCode == 328100 && $customer_policynumber != "" && $customer_email == "" && $customer_phonenumber != ""){
                $payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($payer_message, $MSISDN);
                $customer_message = "Hi $customer_name, this is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successful. For an acknowledgement receipt, click on this link https://jubileeinsure.com/email_form. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($customer_message, $customer_phonenumber);
                $this->sendMail($customer_name, $customer_email, $pdfFilePath);
                //$this->Mv_paybill_model->insertCustomerSMS($MSISDN,$payer_message,$customer_message,$customer_name,$customer_phonenumber,$BillRefNumber);

                //No policy, No Email, No Phonenumber
            }

            /*

            else if($BusinessShortCode == 328100 && $customer_policynumber == "" && $customer_email == "" && $customer_phonenumber == ""){
                $payer_message = "Dear $payer_name, this is to kindly let you know that the policy number $BillRefNumber input for your payment worth KES. $sms_amount is invalid. Kindly confirm your policy number at any of our closest branches. In case of any query feel free to reach us on 0709 949000 or email us customerservice@jubileekenya.com";
                $this->ci_infobip->sendsms($payer_message, $MSISDN);
                $this->sendMail($customer_name, $customer_email, $pdfFilePath);

                //Yes policy, Yes Email, No Phonenumber
            }

            */


            else if($BusinessShortCode == 328100 && $customer_policynumber != "" && $customer_email != "" && $customer_phonenumber == ""){
                $customer_message = "Hi $customer_name, this is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successful. For an acknowledgement receipt, click on this link https://jubileeinsure.com/email_form. Thank you for choosing Jubilee Insurance.
                                    Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($customer_message, $customer_phonenumber);
                //$this->ci_infobip->sendsms($payer_message, 254701521860);
            }

            /*general and medical policies validation*/
            else if($BusinessShortCode == 328102){
                $payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($payer_message, $MSISDN);
                //$this->Mv_paybill_model->insertCustomerSMS($MSISDN,$payer_message,$BillRefNumber);
            }

            /*pension policies validation*/
            else if($BusinessShortCode == 328103){
                $payer_message = "Hi $payer_name, This is to confirm that the payment of KES. $sms_amount for policy number $BillRefNumber has been successfully completed. Thank you for choosing Jubilee Insurance.";
                $this->ci_infobip->sendsms($payer_message, $MSISDN);

            }

            //save payment information
            $this->Mv_paybill_model->insertPaybillPayment($MSISDN, $customer_name, $agent_number, $agent_code, $agent_name, $FirstName, $MiddleName, $LastName, $TransType, $TransID, $TransTime, $TransAmount, $BusinessShortCode, $BillRefNumber, $OrgAccountBalance, $sender_ip_address, $payer_message,$customer_message,$customer_phonenumber);
        }
    }


    public function randonNumber(){
        //Random number genration
        $today = date('YmdHi');
        $startDate = date('YmdHi', strtotime('2012-03-14 09:06:00'));
        $range = $today - $startDate;
        $rand = rand(0, $range);
        echo $startDate + $rand;
    }


    public function sendMail($customer_name, $customer_email, $pdfFilePath){
        // Set your email information
        $from = [
            'email' => 'jubileedeveloper@gmail.com',
            'name' => 'Jubilee Insure'
        ];

        //$to = array('lawiinnocent@gmail.com');
        $to = array($customer_email);
        $subject = 'Jubilee Insurance Acknowledgement Receipt';
        //  $message = 'Type your gmail message here';

        $mail_data = [
            "customerName" => $customer_name,
        ];

        //load the view and save it into $html variable
        $message=$this->load->view('email/customer_email', $mail_data, true);
        // Load CodeIgniter Email library
        $this->load->library('email');
        // Sometimes you have to set the new line character for better result
        $this->email->set_newline("\r\n");
        // Set email preferences
        $this->email->from($from['email'], $from['name']);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->attach($pdfFilePath);
        // Ready to send email and check whether the email was successfully sent
        if (!$this->email->send()) {
            // Raise error message
            // show_error($this->email->print_debugger());
            $SendMail = $this->email->print_debugger();
            if (!is_dir('./email_logs')) {
                mkdir('./email_logs', 0777, true);
            }
            $name = "./email_logs/log-" . time() . ".txt";
            $f = fopen("$name", "w");
            fwrite($f, $SendMail);
            fclose($f);
        } else {
            // Show success notification or other things here
            echo 'Email has been sent';
        }
    }

    public function packageSavePaymentValidation($postdata) {

        $transaction_details = array(
            'TransType' => NULL,
            'TransID' => NULL,
            'TransTime' => NULL,
            'TransAmount' => NULL,
            'BusinessShortCode' => NULL,
            'BillRefNumber' => NULL,
            'MSISDN' => NULL,
            'FirstName' => NULL,
            'LastName' => NULL,
            'MiddleName' => NULL
        );

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $postdata);
        $xml = simplexml_load_string($xml);

        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        if ($responseArray) {
            $transaction_details['TransType'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransType'] ? : FALSE;
            $transaction_details['TransID'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransID'] ? : FALSE;
            $transaction_details['TransTime'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransTime'] ? : FALSE;
            $transaction_details['TransAmount'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['TransAmount'] ? : FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['BusinessShortCode'] ? : FALSE;
            $transaction_details['BillRefNumber'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['BillRefNumber'] ? : FALSE;
            $transaction_details['MSISDN'] = $responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['MSISDN'] ? : FALSE;

            foreach ($responseArray['soapenvBody']['ns1C2BPaymentValidationRequest']['KYCInfo'] as $key => $kycinfo_array) {
                foreach ($kycinfo_array as $kycinfo_key => $kycinfo) {
                    $new_kycinfo[] = $kycinfo;
                }
            }

            if (sizeof($new_kycinfo) == 4) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
            }

            if (sizeof($new_kycinfo) == 6) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['MiddleName'] = !empty($new_kycinfo[2]) ? $new_kycinfo[2] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
            }
        }
    }

    public function confirmPayment() {

        $postdata_results = file_get_contents("php://input");
        $postdata = isset($postdata_results) ? $postdata_results : '';
        if (!is_dir('./successful_confirm_payments')) {
            mkdir('./successful_confirm_payments', 0777, true);
        }
        $name = "./successful_confirm_payments/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $postdata);
        fclose($f);

        $response = $this->confirmPaymentAcknowledgementResponse();
        header("Content-type : text/xml");
        echo $response;

        //save payment information
        $this->packageSavePaymentConfirmation($postdata);
    }

    public function confirmTestPayment() {

        $postdata_results = file_get_contents("php://input");
        $postdata = isset($postdata_results) ? $postdata_results : '';
        if (!is_dir('./successful_confirm_payments')) {
            mkdir('./successful_confirm_payments', 0777, true);
        }
        $name = "./successful_confirm_payments/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $postdata);
        fclose($f);

        $response = $this->confirmPaymentAcknowledgementResponse();
        //header("Content-type : text/xml");
        echo $response;
    }

    public function confirmPaymentAcknowledgementResponse($transaction_id = "1234560000007031") {

        $response = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c2b=\"http://cps.huawei.com/cpsinterface/c2bpayment\">"
            . "<soapenv:Header/>"
            . "<soapenv:Body>"
            . "<c2b:C2BPaymentConfirmationResult>C2B Payment Transaction $transaction_id result received.</c2b:C2BPaymentConfirmationResult>"
            . "</soapenv:Body>"
            . "</soapenv:Envelope>";

        return $response;
    }
}