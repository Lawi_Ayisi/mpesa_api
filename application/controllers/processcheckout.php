<?php
error_reporting(0);
function processcheckout($MERCHANT_TRANSACTION_ID, $ENDPOINT,$PASSWORD,$TIMESTAMP,$PAYBILL_NO)
{
    /// SOAP XML needs to be in this variable
    $bod = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:tns="tns:ns" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"><soapenv:Header><tns:CheckOutHeader><MERCHANT_ID>'.$PAYBILL_NO.'</MERCHANT_ID><PASSWORD>'.$PASSWORD.'</PASSWORD><TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP></tns:CheckOutHeader></soapenv:Header><soapenv:Body><tns:transactionConfirmRequest><TRX_ID>?</TRX_ID><MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID></tns:transactionConfirmRequest></soapenv:Body></soapenv:Envelope>';

    if (!is_dir('./successful_online_confirm_payments_request')) {
        mkdir('./successful_online_confirm_payments_request', 0777, true);
    }
    $name = "./successful_online_confirm_payments_request/log-" . time() . ".xml";
    $f = fopen("$name", "w");
    fwrite($f, $bod);
    fclose($f);

try {

$ch = curl_init(); 
curl_setopt($ch, CURLOPT_URL, $ENDPOINT); 
curl_setopt($ch, CURLOPT_HEADER, 0); 

                        
curl_setopt($ch, CURLOPT_VERBOSE, '0');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_POSTFIELDS, $bod); 
curl_setopt($ch, CURLOPT_TIMEOUT, 60); 
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '0');
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0');

$output = curl_exec($ch);




// Check if any error occured
if(curl_errno($ch))
{
    echo 'Error no : '.curl_errno($ch).' Curl error: ' . curl_error($ch);
}
curl_close($ch);
print_r($output);

    if (!is_dir('./successful_online_confirm_payments_response')) {
        mkdir('./successful_online_confirm_payments_response', 0777, true);
    }
    $name = "./successful_online_confirm_payments_response/log-" . time() . ".xml";
    $f = fopen("$name", "w");
    fwrite($f, $output);
    fclose($f);

    $response = $this->confirmPaymentAcknowledgementResponse();
    header("Content-type : text/xml");
    echo $response;

    //save payment information
    $this->packageSavePaymentConfirmation($output);


} catch (SoapFault $fault) {
    echo $fault;
}
}



?>