<div class="login-box">

    <div class="login-logo"><b><?php echo $site_name; ?></b></div>

    <div class="login-box-body">
        <p class="login-box-msg">Sign Up to get your e-receipt</p>
        <?php echo $form->open(); ?>
        <?php echo $form->messages(); ?>
        <?php echo $form->bs3_text('Firstname', 'firstname'); ?>
        <?php echo $form->bs3_text('Lastname', 'lastname'); ?>
        <?php echo $form->bs3_text('Email', 'email'); ?>
        <?php echo $form->bs3_text('Phonenumber', 'phonenumber' ); ?>
        <div class="row">
            <div class="col-xs-12">
                <?php echo $form->bs3_submit('Sign Up', 'btn btn-primary btn-block btn-flat'); ?>
            </div>
        </div>
        <?php echo $form->close(); ?>
    </div>

</div>