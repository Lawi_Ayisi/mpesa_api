<div class="container">
    <div id="contact_form" class="row">
        <div class="col-md-12">
            <h2>Pay Via MPESA</h2>
            <form role="form" id="feedbackForm"  data-toggle="validator" data-disable="false">
                <div class="form-group">
                    <label class="control-label" for="name">Customer Name *</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="name" name="name" placeholder="Enter your name" required/>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked form-control-feedback"></i></span>
                    </div>
                    <span class="help-block" style="display: none;">Please enter your name.</span>
                </div>

                <div class="form-group">
                    <label class="control-label" for="phone">Phone</label>
                    <input type="tel" class="form-control" id="phone" name="phone" placeholder="Enter your phone"/>
                    <span class="help-block" style="display: none;">Please enter a valid phone number.</span>
                </div>
                <div class="form-group">
                    <label class="control-label" for="amount">Amount To Pay *</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="amount" name="amount" placeholder="Please enter Amount" required/>
                        <span class="input-group-addon"><i class="glyphicon glyphicon-unchecked form-control-feedback"></i></span>
                    </div>
                    <span class="help-block" style="display: none;">Please enter Amount</span>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="your_site_key"></div>
                    <span class="help-block" style="display: none;">Please check that you are not a robot.</span>
                </div>
                <span class="help-block" style="display: none;">Please enter a the security code.</span>
                <button type="submit" id="feedbackSubmit" class="btn btn-primary btn-lg" data-loading-text="Sending..." style="display: block; margin-top: 10px;">Pay Now</button>
            </form>
        </div><!--/span-->
    </div><!--/row-->
    <hr>
</div>