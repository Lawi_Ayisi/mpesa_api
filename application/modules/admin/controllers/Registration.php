<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Registration extends MY_Controller {

    public function index()
    {
        $this->load->library('form_builder');
        $form = $this->form_builder->create_form();

        if ($form->validate())
        {
            // passed validation
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $email = $this->input->post('email');
            $phonenumber = $this->input->post('phonenumber');

            if ($this->ion_auth->login($firstname, $lastname, $email, $phonenumber))
            {
                // login succeed
                $messages = $this->ion_auth->messages();
                $this->system_message->set_success($messages);
                redirect($this->mModule);
            }
            else
            {
                // login failed
                $errors = $this->ion_auth->errors();
                $this->system_message->set_error($errors);
                refresh();
            }
        }

        // display form when no POST data, or validation failed
        $this->mViewData['form'] = $form;
        $this->mBodyClass = 'registration-page';
        $this->render('registration', 'empty');
    }

}
