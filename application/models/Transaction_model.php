<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paybill_model
 *
 * @author epic-code
 */
class Transaction_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getPaybillPayments($payment_id = FALSE, $start_at = 0, $limit = 10000, $voucher_not_processed = FALSE) {

        $results = array();

        if ($payment_id) {
            $this->db->where("paybill_payments_id", $payment_id);
        }

        if ($limit) {
            $this->db->limit($limit, $start_at);
        }
        if ($voucher_not_processed) {
            $this->db->where("voucher_processed", NULL);
        }

        $this->db->order_by(1, 'desc');

        $data = $this->db->get("mv_paybill_payments ppp");

        if ($data->num_rows()) {
            foreach ($data->result() as $row) {
                $results[] = $row;
            }
        }

        return $results;
    }

    function insertPaybillPayment($TransType, $TransID, $TransTime, $TransAmount, $BusinessShortCode, $BillRefNumber, $OrgAccountBalance, $MSISDN, $FirstName, $LastName, $sender_ip_address) {

        $param = array(
            'transaction_type' => $TransType,
            'transaction_id' => $TransID,
            'transaction_time' => $TransTime,
            'transaction_amount' => $TransAmount,
            'business_shortcode' => $BusinessShortCode,
            'bill_reference_number' => preg_replace('/\s+/', '', ucfirst(strtolower($BillRefNumber))),
            'organisation_account_balance' => $OrgAccountBalance,
            'phonenumber' => $MSISDN,
            'firstname' => $FirstName,
            'lastname' => $LastName,
            'date_added' => date('Y-m-d'),
            'sender_ip_address' => $sender_ip_address
        );

        return $this->db->insert("transactions", $param);
    }

    function updatePaybillPayment($paybill_payments_id, $update_param) {
        $this->db->where('paybill_payments_id', $paybill_payments_id);
        return $this->db->update('mv_paybill_payments', $update_param);
    }

    function updateMpesaPaybillPayment($paybill_payments_id, $mobile_voucher) {

        $table_name = "mv_paybill_payments";

        $param = array(
            'voucher_processed' => 1,
            'timestamp_voucher_processed' => time(),
            'voucher_code_issued' => $mobile_voucher
        );
        $this->db->where('paybill_payments_id', $paybill_payments_id);
        return $this->db->update($table_name, $param);
    }

}

?>
