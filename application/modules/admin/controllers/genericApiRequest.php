<?php
error_reporting(0);
include_once "B2CCredentials.php";
defined('BASEPATH') OR exit('No direct script access allowed');

class GenericApiRequest extends Admin_Controller{
    public $organisationShortCode;
    public $organisationName;
    public $randomNumber;
    public $OriginatorConversationID;
    private $_OriginatorConversationID;
    private $_show_output = 0;

    public function __construct(){
        $this->organisationName = 'JUBILEEINSURANCE';
        $this->organisationShortCode = '902004';
        $this->OriginatorConversationID = $this->organisationShortCode . '_' . $this->organisationName . '_' .time();
    }

    public function index(){
        echo $this->OriginatorConversationID;
    }

    public function Request ( ) {
        // echo $this->OriginatorConversationID;
        $B2CCredentials                   = new B2CCredentials(  );
        //$TransactionDetails               = self::getTransactionDetails($this->OriginatorConversationID);
        //$this->_OriginatorConversationID  = $OriginatorConversationID;

        //$receiver_cellphone  =  isset($TransactionDetails['transaction_to']) ? $TransactionDetails['transaction_to'] : null;
        //$receiver_amount     =  isset($TransactionDetails['receiver_amount']) ? $TransactionDetails['receiver_amount'] : null;
        $receiver_cellphone = 254713171292;
        $receiver_amount = 100.00;


        if($this->_show_output){
            echo "\$OriginatorConversationID={$this->OriginatorConversationID} <br>";
            echo "\$receiver_cellphone={$receiver_cellphone} <br>";
            echo "\$receiver_amount={$receiver_amount} <br>";

            echo '<pre>';
            print_r($B2CCredentials);
            //print_r($TransactionDetails);
            echo '</pre>';

        }

        $xml = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:req=\"http://api-v1.gen.mm.vodafone.com/mminterface/request\">" .
            "<soapenv:Header>" .
            "<tns:RequestSOAPHeader xmlns:tns=\"http://www.huawei.com/schema/osg/common/v2_1\">" .
            "<tns:spId>{$B2CCredentials->spId}</tns:spId>" .
            "<tns:spPassword>{$B2CCredentials->spPassword}</tns:spPassword>" .
            "<tns:timeStamp>{$B2CCredentials->timeStamp}</tns:timeStamp>" .
            "<tns:serviceId>{$B2CCredentials->serviceId}</tns:serviceId>" .
            "</tns:RequestSOAPHeader>" .
            "</soapenv:Header>" .
            "<soapenv:Body>" .
            "<req:RequestMsg><![CDATA[<?xml version=\"1.0\" encoding=\"UTF-8\" ?><request xmlns=\"http://api-v1.gen.mm.vodafone.com/mminterface/request\">" .
            "<Transaction>" .
            "<CommandID>BusinessPayment</CommandID>" .
            "<LanguageCode>0</LanguageCode>" .
            "<OriginatorConversationID>{$this->OriginatorConversationID}</OriginatorConversationID>" .
            "<Remark>0</Remark>" .
            "<Parameters><Parameter>" .
            "<Key>Amount</Key>" .
            "<Value>{$receiver_amount}</Value>" .
            "</Parameter></Parameters>" .
            "<ReferenceData>" .
            "<ReferenceItem>" .
            "<Key>QueueTimeoutURL</Key>" .
            "<Value>{$B2CCredentials->QueueTimeoutURL}</Value>" .
            "</ReferenceItem></ReferenceData>" .
            "<Timestamp>" . date("c", time()) . "</Timestamp>" .
            "</Transaction>" .
            "<Identity>" .
            "<Caller>" .
            "<CallerType>2</CallerType>" .
            "<ThirdPartyID>" . $B2CCredentials->shortcode . "</ThirdPartyID>" .
            "<Password>Password0</Password>" .
            "<CheckSum>CheckSum0</CheckSum>" .
            "<ResultURL>{$B2CCredentials->ResultURL}</ResultURL>" .
            "</Caller>" .
            "<Initiator>" .
            "<IdentifierType>11</IdentifierType>" .
            "<Identifier>jaribioinit2</Identifier>" .
            "<SecurityCredential>{$B2CCredentials->SecurityCredential}</SecurityCredential>" .
            "<ShortCode>902004</ShortCode>" .
            "</Initiator><PrimaryParty><IdentifierType>4</IdentifierType><Identifier>" . $B2CCredentials->shortcode . "</Identifier></PrimaryParty><ReceiverParty><IdentifierType>1</IdentifierType><Identifier>{$receiver_cellphone}</Identifier>" .
            "</ReceiverParty>" .
            "<AccessDevice>" .
            "<IdentifierType>1</IdentifierType>" .
            "<Identifier>Identifier3</Identifier>" .
            "</AccessDevice></Identity><KeyOwner>1</KeyOwner></request>]]></req:RequestMsg></soapenv:Body></soapenv:Envelope>";

        if (!is_dir('./B2C_Transfer_Requests')) {
            mkdir('./B2C_Transfer_Requests', 0777, true);
        }
        $name = "./B2C_Transfer_Requests/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $xml);
        fclose($f);

//   B2C Transaction Request Logs

        $response =  self::sendMPESATransactionRequest($xml);
//    $response =  self::sendMPESATransactionRequestSSL($xml);

//   B2C Transaction Response Logs

//    if ($this->_show_output ){ echo '<pre>' ; print_r($response); echo '</pre>'; }

        if ($response['success'] == 0){
            //queue for resend or queue for querrying
        }else{



            $ResponseCode          =  $response['ResponseCode'];

            $GAPIResponseCodes     =  self::getResponseCode($ResponseCode);
            $GAPIResponseDesc      =  isset($GAPIResponseCodes['desc']) ? $GAPIResponseCodes['desc'] : null;
            $GAPIResponseState     =  isset($GAPIResponseCodes['state']) ? $GAPIResponseCodes['state'] : null;
            $GAPIResponseRetry     =  isset($GAPIResponseCodes['retry']) ? $GAPIResponseCodes['retry'] : null;

            if ($this->_show_output ){
                echo '<pre>';
                print_r($GAPIResponseCodes);
                echo '</pre>';
            }

            if ($this->_show_output ){
                echo "\$ResponseCode={$ResponseCode}<br>\r\n";
                echo "\$GAPIResponseDesc={$GAPIResponseDesc}<br>\r\n";
                echo "\$GAPIResponseState={$GAPIResponseState}<br>\r\n";
                echo "\$GAPIResponseRetry={$GAPIResponseRetry}<br>\r\n";
            }



            if($GAPIResponseState==1){
                self::successJubileeTransaction( $GAPIResponseState , $ResponseCode, $GAPIResponseDesc , $response['OriginatorConversationID'] , $response['ConversationID'] );
            }else{
                self::failedJubInsureTransaction($GAPIResponseState , $ResponseCode, $GAPIResponseDesc , $GAPIResponseRetry ,  $response['OriginatorConversationID'] , $response['ConversationID']);
            }

        }
        exit();

    }

    public function sendMPESATransactionRequest($request) {
        $B2CCredentials = new B2CCredentials();

        $request = trim(preg_replace('/\s+/', ' ', $request));
        $contentLength = strlen($request);
        $headers =  self::getRequestHeaders($contentLength);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $B2CCredentials->EndPointGARequest);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request); // the SOAP request
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($this->_show_output ){
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $curlLogFile = 'logprd'.date('His').'.log';
            $verbose     = fopen($curlLogFile, 'ab');
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
        }

        // converting
        $response = curl_exec($ch);
        curl_close($ch);

        if (!is_dir('./B2C_Transfer_Response')) {
            mkdir('./B2C_Transfer_Response', 0777, true);
        }
        $name = "./B2C_Transfer_Response/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $response);
        fclose($f);


        $getGenericAPIRequest_SoapRequest =  self::getGenericAPIResult($response, 'http://api-v1.gen.mm.vodafone.com/mminterface/request');
        header("Content-type : text/xml");
        echo $response;
//    return $getGenericAPIRequest_SoapRequest;
        return $response;
    }

    private function getRequestHeaders($contentLength) {
        return array(
            'Content-type: text/xml;charset="utf-8"',
            'Accept: text/xml',
            'Cache-Control: no-cache',
            'Pragma: no-cache',
//            'SOAPAction: ' . QUERY_MPESA_TRANSACTION_URL,
            'Content-length: ' . $contentLength
        );
    }

    private function getGenericAPIResult( $response , $nameSpace = 'http://api-v1.gen.mm.vodafone.com/mminterface/request' ){

        if(empty($response) || !strstr($response, 'ResponseMsg')){
            return array(
                'success'                   => 0 ,
                'ResponseCode'              => null ,
                'ResponseDesc'              => null ,
                'ConversationID'            => null ,
                'OriginatorConversationID'  => null ,
                'ServiceStatus'             => null
            );
        }

        $soap = simplexml_load_string($response, null, LIBXML_NOCDATA);
        $soap->registerXPathNamespace('ns1', $nameSpace);
        $Response      = simplexml_load_string((list($s) = $soap->xpath('//ns1:ResponseMsg')) ? $s : '');
        $json          = json_encode($Response);
        $responseArray = json_decode($json,true);

        $ResponseCode              = isset($responseArray['ResponseCode']) ? $responseArray['ResponseCode'] : null;
        $ResponseDesc              = isset($responseArray['ResponseDesc']) ? $responseArray['ResponseDesc'] : null;
        $ConversationID            = isset($responseArray['ConversationID']) && is_string($responseArray['ConversationID']) ? $responseArray['ConversationID'] : null;
        $OriginatorConversationID  = isset($responseArray['OriginatorConversationID'])  && is_string($responseArray['OriginatorConversationID']) ?$responseArray['OriginatorConversationID'] : $this->_OriginatorConversationID;
        $ServiceStatus             = isset($responseArray['ServiceStatus']) ? $responseArray['ServiceStatus'] : null;

        if ($this->_show_output ){
            echo "\$ResponseCode={$ResponseCode}  \r\n";
            echo "\$ResponseDesc={$ResponseDesc}  \r\n";
            echo "\$ConversationID={$ConversationID} \r\n";
            echo "\$OriginatorConversationID={$OriginatorConversationID} \r\n";
            echo "\$ServiceStatus={$ServiceStatus} \r\n";
        }

        return array(
            'success'                   => 1 ,
            'ResponseCode'              => $ResponseCode ,
            'ResponseDesc'              => $ResponseDesc ,
            'ConversationID'            => $ConversationID ,
            'OriginatorConversationID'  => $OriginatorConversationID ,
            'ServiceStatus'             => $ServiceStatus ,
        );

    }

    private function  getResponseCode( $ResponseCode ){
        $ResponseCodes[0]              = array( 'desc'=>'Success' , 'state'=> 1 ,'retry'=> 0);
        $ResponseCodes['0']            = array( 'desc'=>'Success' , 'state'=> 1 ,'retry'=> 0);
        $ResponseCodes['000000000']    = array( 'desc'=>'Success' , 'state'=> 1 ,'retry'=> 0);
        $ResponseCodes['100000000']    = array( 'desc'=>'Request was cached, waiting for resending' , 'state'=> 0 ,'retry'=> 0);
        $ResponseCodes['100000001']    = array( 'desc'=>'The system is overload' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000002']    = array( 'desc'=>'Throttling error' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000003']    = array( 'desc'=>'Exceed the limitation of the LICENSE' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000004']    = array( 'desc'=>'Internal Server Error' ,'state'=>  0, 'retry'=> 1) ;
        $ResponseCodes['100000005']    = array( 'desc'=>'Invalid input value:%1 %1 indicates the parameter’s name.' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000006']    = array( 'desc'=>'SP’s status is abnormal' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000007']    = array( 'desc'=>'Authentication failed' , 'state'=> 0 ,'retry'=> 0);
        $ResponseCodes['100000008']    = array( 'desc'=>'Service’s status is abnormal' ,'state'=>  0 ,'retry'=> 1);
        $ResponseCodes['100000009']    = array( 'desc'=>'API’s status is abnormal' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000010']    = array( 'desc'=>'Insufficient permissions' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000011']    = array( 'desc'=>'Exceed the limitation of request rate' , 'state'=> 0 ,'retry'=> 0);
        $ResponseCodes['100000012']    = array( 'desc'=>'Insufficient balance' , 'state'=> 0 ,'retry'=> 0);
        $ResponseCodes['100000013']    = array( 'desc'=>'No route' , 'state'=> 0 ,'retry'=> 1);
        $ResponseCodes['100000014']    = array( 'desc'=>'Missing mandatory parameter:%1 %1 indicates the parameter’s name.' , 'state'=> 0 ,'retry'=> 0);
        $ResponseCodes['100000018']    = array( 'desc'=>'InitiatorCredentialCheckFailure' , 'state'=> 0 ,'retry'=> 0);

        //other result codes
        $ResponseCodes[1]    = array(  'desc'=>'Insufficient Funds' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[2]    = array(  'desc'=>'Less Than Minimum Transaction Value' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[3]    = array(  'desc'=>'More Than Maximum Transaction Value' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[4]    = array(  'desc'=>'Would Exceed Daily Transfer Limit' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[5]    = array(  'desc'=>'Would Exceed Minimum Balance' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[6]    = array(  'desc'=>'Unresolved Primary Party' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[7]    = array(  'desc'=>'Unresolved Receiver Party' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[8]    = array(  'desc'=>'Would Exceed Maxiumum Balance' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[11]   = array(  'desc'=>'Debit Account Invalid' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[12]   = array(  'desc'=>'Credit Account Invaliud' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[13]   = array(  'desc'=>'Unresolved Debit Account' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[14]   = array( 'desc'=> 'Unresolved Credit Account' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes[15]   = array(  'desc'=>'Duplicate Detected' , 'state'=> 1 ,'retry'=> 0 );
        $ResponseCodes[17]   = array(  'desc'=>'Internal Failure' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes[18]   = array(  'desc'=>'Initiator Credential Check Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[19]   = array(  'desc'=>'Message Sequencing Failure' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes[20]   = array(  'desc'=>'Unresolved Initiator' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[21]   = array(  'desc'=>'Initiator to Primary Party Permission Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[22]   = array(  'desc'=>'Initiator to Receiver Party Permission Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[23]   = array(  'desc'=>'Request schema validation error' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes[24]   = array(  'desc'=>'Missing mandatory fields' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes[25]   = array(  'desc'=>'Cannot communicate with Caller' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes[26]   = array(  'desc'=>'Traffic blocking condition in place' , 'state'=> 0 ,'retry'=> 0 );

        //other result codes
        $ResponseCodes['1']    = array(  'desc'=>'Insufficient Funds' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['2']    = array(  'desc'=>'Less Than Minimum Transaction Value' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['3']    = array(  'desc'=>'More Than Maximum Transaction Value' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['4']    = array(  'desc'=>'Would Exceed Daily Transfer Limit' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['5']    = array(  'desc'=>'Would Exceed Minimum Balance' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['6']    = array(  'desc'=>'Unresolved Primary Party' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['7']    = array(  'desc'=>'Unresolved Receiver Party' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['8']    = array(  'desc'=>'Would Exceed Maxiumum Balance' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['11']    = array(  'desc'=>'Debit Account Invalid' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['12']    = array(  'desc'=>'Credit Account Invalid' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['13']    = array(  'desc'=>'Unresolved Debit Account' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['14']    = array( 'desc'=> 'Unresolved Credit Account' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes['15']    = array(  'desc'=>'Duplicate Detected' , 'state'=> 1 ,'retry'=> 0 );
        $ResponseCodes['17']    = array(  'desc'=>'Internal Failure' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes['18']    = array(  'desc'=>'Initiator Credential Check Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['19']    = array(  'desc'=>'Message Sequencing Failure' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes['20']    = array(  'desc'=>'Unresolved Initiator' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['21']    = array(  'desc'=>'Initiator to Primary Party Permission Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['22']    = array(  'desc'=>'Initiator to Receiver Party Permission Failure' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['23']    = array(  'desc'=>'Request schema validation error' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes['24']    = array(  'desc'=>'Missing mandatory fields' , 'state'=> 0 ,'retry'=> 1 );
        $ResponseCodes['25']    = array(  'desc'=>'Cannot communicate with Caller' , 'state'=> 0 ,'retry'=> 0 );
        $ResponseCodes['26']    = array(  'desc'=>'Traffic blocking condition in place' , 'state'=> 0 ,'retry'=> 0 );


        if(isset($ResponseCodes[$ResponseCode])){
            return $ResponseCodes[$ResponseCode];
        }

        return array( 'desc'=>null , 'state'=> null ,'retry'=> null);

    }

    private function successJubileeTransaction(  $GAPIResponseState , $ResponseCode,  $GAPIResponseDesc ,  $OriginatorConversationID , $ConversationID ){
        global $db;

        //	require_once(ROOT_PATH . 'includes/db.php');
// 	$db->debug=0;

        /*$update_sql = "
        UPDATE TRANSLINES
        SET MPESASUCCESS=1,
        RETRY=0,
        MPESARCODE='{$ResponseCode}' ,
        MPESARDESC='{$GAPIResponseDesc}',
        MPESACID='{$ConversationID}'
        WHERE CONVERSATIONID='{$OriginatorConversationID}'
        ";
        $db->Execute( $update_sql);
        return true;
        */

        //save b2c payment information
        $this->load->model('Mv_paybill_model');
        $this->Mv_paybill_model->insertB2CPayment($ResponseCode,$GAPIResponseDesc,$ConversationID,$OriginatorConversationID);
        $B2CResponse = $this->confirmPayment();
        return $B2CResponse;
    }

    private function failedJubInsureTransaction( $GAPIResponseState , $ResponseCode, $GAPIResponseDesc , $GAPIResponseRetry ,   $OriginatorConversationID ,  $ConversationID){
        global $db;

        //require_once(ROOT_PATH . 'includes/db.php');

        $update_sql = "
 	UPDATE TRANSLINES SET 
 	MPESASUCCESS=0, 
 	RETRY={$GAPIResponseRetry},
 	MPESARCODE='{$GAPIResponseState}' ,
 	MPESARDESC='{$GAPIResponseDesc}'
 	WHERE CONVERSATIONID='{$OriginatorConversationID}'
 	";

        //$db->Execute( $update_sql);

        echo var_dump($update_sql);

        // :/
        return true;
    }

    public function confirmPayment() {

        $postdata_results = file_get_contents("php://input");
        $postdata = isset($postdata_results) ? $postdata_results : '';
        if (!is_dir('./successful_B2C_confirm_payments')) {
            mkdir('./successful_B2C_confirm_payments', 0777, true);
        }
        $name = "./successful_B2C_confirm_payments/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $postdata);
        fclose($f);

        $response = $this->confirmB2CPaymentAcknowledgementResponse();
        header("Content-type : text/xml");
        echo $response;
    }

    public function confirmB2CPaymentAcknowledgementResponse() {

        $response = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:req=\"http://api-v1.gen.mm.vodafone.com/mminterface/request\">"
            . "<soapenv:Body>"
            . "<ns2:ResponseMsg xmlns:ns2=\"http://api-v1.gen.mm.vodafone.com/mminterface/result\"><![CDATA[<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
            . "<response xmlns=\"http://api-v1.gen.mm.vodafone.com/mminterface/response\">"
            . "<ResponseCode>00000000</ResponseCode>"
            . "<ResponseDesc>success</ResponseDesc>"
            . "<ConversationID>"
            . "</ConversationID>"
            . "<OriginatorConversationID>"
            . "</OriginatorConversationID>"
            . "<ServiceStatus></ServiceStatus>"
            . "</response>]]></ns2:ResponseMsg>"
            . "</soapenv:Body>"
            . "</soapenv:Envelope>";

        if (!is_dir('./successful_confirm_B2C_payments')) {
            mkdir('./successful_confirm_B2C_payments', 0777, true);
        }
        $name = "./successful_confirm_B2C_payments/log-" . time() . ".xml";
        $f = fopen("$name", "w");
        fwrite($f, $response);
        fclose($f);

        return $response;
    }

    public function packageSaveB2CPaymentConfirmation($postdata) {
        $transaction_details = array(
            'TransactionType' => NULL,
            'TransID' => NULL,
            'TransTime' => NULL,
            'TransAmount' => NULL,
            'BusinessShortCode' => NULL,
            'BillRefNumber' => NULL,
            'MSISDN' => NULL,
            'FirstName' => NULL,
            'LastName' => NULL,
            'MiddleName' => NULL
        );

        $xml = preg_replace("/(<\/?)(\w+):([^>]*>)/", "$1$2$3", $postdata);
        $xml = simplexml_load_string($xml);

        $json = json_encode($xml);
        $responseArray = json_decode($json, true);

        if ($responseArray) {
            $transaction_details['TransType'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransType'] ?: FALSE;
            $transaction_details['TransID'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransID'] ?: FALSE;
            $transaction_details['TransTime'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransTime'] ?: FALSE;
            $transaction_details['TransAmount'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['TransAmount'] ?: FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BusinessShortCode'] ?: FALSE;
            $transaction_details['BillRefNumber'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BillRefNumber'] ?: FALSE;
            $transaction_details['OrgAccountBalance'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['OrgAccountBalance'] ?: FALSE;
            $transaction_details['BusinessShortCode'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['BusinessShortCode'] ?: FALSE;
            $transaction_details['MSISDN'] = $responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['MSISDN'] ?: FALSE;
            foreach ($responseArray['soapenvBody']['ns1C2BPaymentConfirmationRequest']['KYCInfo'] as $key => $kycinfo_array) {
                foreach ($kycinfo_array as $kycinfo_key => $kycinfo) {
                    $new_kycinfo[] = $kycinfo;
                }
            }
            if (sizeof($new_kycinfo) == 4) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
            }

            if (sizeof($new_kycinfo) == 6) {
                $transaction_details['FirstName'] = !empty($new_kycinfo[1]) ? $new_kycinfo[1] : FALSE;
                $transaction_details['MiddleName'] = !empty($new_kycinfo[3]) ? $new_kycinfo[3] : FALSE;
                $transaction_details['LastName'] = !empty($new_kycinfo[5]) ? $new_kycinfo[5] : FALSE;
            }

            $TransType = $transaction_details['TransType'];
            $TransID = $transaction_details['TransID'];
            $TransTime = $transaction_details['TransTime'];
            $TransAmount = $transaction_details['TransAmount'];
            $BusinessShortCode = $transaction_details['BusinessShortCode'];
            $BillRefNumber = $transaction_details['BillRefNumber'];
            $OrgAccountBalance = $transaction_details['OrgAccountBalance'];
            $MSISDN = $transaction_details['MSISDN'];
            $FirstName = $transaction_details['FirstName'];
            $MiddleName = $transaction_details['MiddleName'];
            $LastName = $transaction_details['LastName'];
            $sender_ip_address = $this->input->ip_address();
        }
    }

    public function sendMPESATransactionRequestSSL($request) {
        $B2CCredentials =  new B2CCredentials();
        $request        =  trim(preg_replace('/\s+/', ' ', $request));
        $contentLength  =  strlen($request);
        $headers        =  self::getRequestHeaders($contentLength);

        $ch = curl_init ();

        curl_setopt ($ch, CURLOPT_URL, $B2CCredentials->EndPointGARequestSSL);
        curl_setopt ($ch, CURLOPT_PROTOCOLS, CURLPROTO_HTTPS);
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_VERBOSE, 0);
        curl_setopt ($ch, CURLOPT_POST, 1);//POST
//   curl_setopt ($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt ($ch, CURLOPT_TIMEOUT, 120);

        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);

        //curl_setopt ($ch, CURLOPT_CAINFO,  $B2CCredentials->cainfo );
        curl_setopt ($ch, CURLOPT_SSLCERT, $B2CCredentials->sslcert );
        curl_setopt ($ch, CURLOPT_SSLKEY,  $B2CCredentials->sslkey );

        curl_setopt ($ch, CURLOPT_POSTFIELDS, $request);
        curl_setopt ($ch, CURLOPT_HTTPHEADER, $headers);

        if ($this->_show_output ){
            curl_setopt($ch, CURLOPT_VERBOSE, true);
            $curlLogFile = 'logprd'.date('His').'.log';
            $verbose     = fopen($curlLogFile, 'ab');
            curl_setopt($ch, CURLOPT_STDERR, $verbose);
        }

        $response = curl_exec($ch);

        curl_close($ch);



        if ($this->_show_output ){
            var_dump($response);
        }

        $getGenericAPIRequest_SoapRequest =  self::getGenericAPIResult($response, 'http://api-v1.gen.mm.vodafone.com/mminterface/request');
        header("Content-type : text/xml");
        return $getGenericAPIRequest_SoapRequest;
    }

    private function getTransactionDetails( $OriginatorConversationID ) {
        global $db;

        require_once(ROOT_PATH . 'includes/db.php');

        $db->debug=0;

        $sql = "
 	select T.MEMBER_ID , T.TRANSREF, T.CONVERSATIONID, T.ITEMTO TRANSACTION_TO, M.EMAIL SENDOR_EMAIL , 
M.FIRSTNAME SENDOR_FIRSTNAME ,	M.FULLNAME SENDOR_FULLNAME , R.CELLPHONE RECEIVER_CELLPHONE , concat(C.CALLINGCODE,R.CELLPHONE) RECEIVER_CELLPHONE_FMT,
R.EMAIL RECEIVER_EMAIL, R.FULLNAMES RECEIVER_FULLNAMES , T.RECEIVED_AMOUNT RECEIVER_AMOUNT , R.COUNTRYCODE , C.CALLINGCODE
from TRANSLINES T  
inner join MEMBERS M on M.ID = T.MEMBER_ID 
inner join RECEIPIENTS R on R.MEMBER_ID = T.MEMBER_ID  
inner join COUNTRIES C on C.COUNTRYCODE=R.COUNTRYCODE
where T.CONVERSATIONID='{$OriginatorConversationID}' and concat(C.CALLINGCODE,R.CELLPHONE) =T.ITEMTO
";

        $TransactionDetails = $db->GetRow( $sql );

        return array(
            'found'               => isset($TransactionDetails) && is_array($TransactionDetails) && sizeof($TransactionDetails)>0 ? 1 : 1,
            'member_id'           => isset($TransactionDetails['MEMBER_ID']) ? $TransactionDetails['MEMBER_ID'] : null,
            'transref'            => isset($TransactionDetails['TRANSREF']) ? $TransactionDetails['TRANSREF'] : null,
            'transaction_to'      => isset($TransactionDetails['TRANSACTION_TO']) ? $TransactionDetails['TRANSACTION_TO'] : null,
            'sendor_email'        => isset($TransactionDetails['SENDOR_EMAIL']) ? $TransactionDetails['SENDOR_EMAIL'] : null,
            'sendor_firstname'    => isset($TransactionDetails['SENDOR_FIRSTNAME']) ? $TransactionDetails['SENDOR_FIRSTNAME'] : null,
            'sendor_fullname'     => isset($TransactionDetails['SENDOR_FULLNAME']) ? $TransactionDetails['SENDOR_FULLNAME'] : null,
            'receiver_cellphone'  => isset($TransactionDetails['RECEIVER_CELLPHONE']) ? $TransactionDetails['RECEIVER_CELLPHONE'] : null,
            'receiver_email'      => isset($TransactionDetails['RECEIVER_EMAIL']) ? $TransactionDetails['RECEIVER_EMAIL'] : null,
            'receiver_fullnames'  => isset($TransactionDetails['RECEIVER_FULLNAMES']) ? $TransactionDetails['RECEIVER_FULLNAMES'] : null,
            'receiver_amount'     => isset($TransactionDetails['RECEIVER_AMOUNT']) ? round($TransactionDetails['RECEIVER_AMOUNT']) : null,
        );

    }

}

 