<?php

final class B2CCredentials {
    public $spId;
    public $serviceId;
    public $spPasswordRaw;
    public $spPassword;
    public $shortcode;
    public $timeStamp;
    public $SecurityCredential;

    public $EndPointQuery;
    public $EndPointQuerySSL;

    public $EndPointGARequest;
    public $EndPointGARequestSSL;

    public $QueueTimeoutURL;
    public $ResultURL;

    //certs
    public $cainfo;
    public $sslcert;
    public $sslkey;
    /**
     * SecurityCredential
     *
     * @var string
     */
    const sckey = '/home/pesazoom/ssl_b2c/ApiCryptPublicOnly_Prod_Initiator.pem';

    public function  __construct(  ){

        $this->timeStamp            = date('YmdHis');
        $this->spId                 = '195';
        $this->serviceId            = '100';
        $this->shortcode            = '80';
        $this->spPasswordRaw        = 'Mih!234';

        $hashData                   = $this->spId . $this->spPasswordRaw . $this->timeStamp;
        $spPasswordHash             = hash('sha256', $hashData, false);
        $this->spPassword           = base64_encode($spPasswordHash);

        $this->SecurityCredential   = self::EncryptData('QWEQWE12324324WEWQRETTYuiTRUYT6578');

        $this->EndPointGARequest    = 'http://196.201.214.137:8310/mminterface/request';
        $this->EndPointGARequestSSL = 'https://196.201.214.137:18423/mminterface/request';

        $this->EndPointQuery        = 'http://196.201.214.137:8310/queryTransactionService/services/transaction';
        $this->EndPointQuerySSL     = 'https://196.201.214.137:18423/queryTransactionService/services/transaction';

        $this->ResultURL            = 'https://72.167.44.129/~pesazoom/prod/api/b2c/result/';
        $this->QueueTimeoutURL      = 'https://72.167.44.129/~pesazoom/prod/api/b2c/timeout/';

//     $this->cainfo               = '/home/pesazoom/ssl_b2c/testbroker.safaricom.com_X509_cert.crt';
        $this->cainfo               = '/home/pesazoom/ssl_b2c/all_cas.crt';//erastus 02OCT14

        $this->sslcert              = '/home/pesazoom/ssl_b2c/certs_chain.pem';
        $this->sslkey               = '/home/pesazoom/ssl_b2c/minuteflash.com_publickey.key';

    }

    function EncryptData($source) {

//    $fp = fopen("/home/pesazoom/ssl_b2c/ApiCryptPublicOnly_Prod_Initiator.pem", "r");
        $fp = fopen( self::sckey, "r");

        $pub_key_string = fread($fp, 8192);
        fclose($fp);
        $key_resource   = openssl_get_publickey($pub_key_string);

        openssl_public_encrypt($source, $crypttext, $key_resource, OPENSSL_PKCS1_PADDING);

        return(base64_encode($crypttext));

    }

}

/*
$B2CCredentials = new B2CCredentials();

echo '<pre>';
 print_r($B2CCredentials);
echo '</pre>';

*/