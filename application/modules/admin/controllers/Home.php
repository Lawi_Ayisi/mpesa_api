<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();
        // only login users can access Admin Panel
        $this->verify_login();
    }

    public function index()
    {
        $this->load->model('Customer_model', 'customers');
        /*General and Medical Business Transactions*/
        $this->db->where('business_shortcode',328102);
        $this->db->from('transactions');
        $generalMedicalCount = $this->db->count_all_results();
        $this->mViewData['genMedDeptCount'] = array(
            'generalMedicalTransactions' => $generalMedicalCount,
        );

        /*Life Business Transactions*/
        $this->db->where('business_shortcode',328100);
        $this->db->from('transactions');
        $lifeCount = $this->db->count_all_results();

        $this->mViewData['lifeDeptCount'] = array(
            'lifeTransactions' => $lifeCount,
        );

        /*Pension Business Transactions*/
        $this->db->where('business_shortcode',328103);
        $this->db->from('transactions');
        $lifeCount = $this->db->count_all_results();
        $this->mViewData['pensionDeptCount'] = array(
            'pensionTransactions' => $lifeCount,
        );
        $this->mViewData['count1'] = array(
            'customers' => $this->customers->count_all(),
        );
        //transaction tab
        $this->load->model('Transaction_model', 'transactions');
        $this->mViewData['count'] = array(
            'transactions' => $this->transactions->count_all(),
        );
        $this->render('home');
    }
}
