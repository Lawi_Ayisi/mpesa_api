/**
 * Created by lawi on 6/21/17.
 */
function captchaCode() {
    var Numb1, Numb2, Numb3, Numb4, Code;
    Numb1 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb2 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb3 = (Math.ceil(Math.random() * 10)-1).toString();
    Numb4 = (Math.ceil(Math.random() * 10)-1).toString();

    Code = Numb1 + Numb2 + Numb3 + Numb4;
    $("#captcha span").remove();
    $("#captcha input").remove();
    $("#captcha").append("<span id='code'>" + Code + "</span><button type='button' class='btn btn-outline-primary' onclick='captchaCode();'>");
}

$(function() {
    captchaCode();

    $('#paymentForm').submit(function(){
        var captchaVal = $("#code").text();
        var captchaCode = $(".captcha").val();
        if (captchaVal == captchaCode) {
            $(".captcha").css({
                "color" : "#609D29"
            });
        }
        else {
            $(".captcha").css({
                "color" : "#CE3B46"
            });
        }

        if ((captchaVal !== captchaCode)) {
            return false;
        }
        if ((captchaVal == captchaCode)) {
            document.getElementById("paymentForm").submit();
            $("#paymentForm").css("display", "none");
            $("#paymentForm").ajaxForm({url: 'https://192.168.50.47/mpesa_api/requestcheckout', type: 'post'});
            //$.post('https://192.168.50.47/mpesa_api/requestcheckout', $('#paymentForm').serialize());
            $("#form").append("<div class='col-md-8 col-md-offset-2'><h2>MPESA payment request has been sent to the customer!</h2>" +
                "<a href='https://192.168.50.47/mpesa_api/customercheckout'>Click, to go back to the payment form. Thank You!</a></div>");

            return false;
        }
    });
});