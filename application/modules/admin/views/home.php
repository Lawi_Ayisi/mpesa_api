<div class="row">

    <!--<div class="col-md-4">
		<?php /*echo modules::run('adminlte/widget/box_open', 'Shortcuts'); */?>
			<?php /*echo modules::run('adminlte/widget/app_btn', 'fa fa-user', 'Account', 'panel/account'); */?>
			<?php /*echo modules::run('adminlte/widget/app_btn', 'fa fa-sign-out', 'Logout', 'panel/logout'); */?>
		<?php /*echo modules::run('adminlte/widget/box_close'); */?>
	</div>-->
    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'red', $lifeDeptCount['lifeTransactions'], 'Life Business Transactions', 'fa fa-bank', 'newpaybill/lifeTransactions'); ?>
    </div>

    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'green', $pensionDeptCount['pensionTransactions'], 'Pension Business Transactions', 'fa fa-bank', 'newpaybill/pensionTransactions'); ?>
    </div>

    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'black', $genMedDeptCount['generalMedicalTransactions'], 'General & Medical Business Transactions', 'fa fa-bank', 'newpaybill/generalMedicalTransactions'); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'green', $count['transactions'], 'Total Transactions', 'fa fa-money', 'newpaybill'); ?>
    </div>

    <div class="col-md-4">
        <?php echo modules::run('adminlte/widget/info_box', 'black', $count1['customers'], 'Customers', 'fa fa-bank', 'newpaybill/customers'); ?>
    </div>
</div>
