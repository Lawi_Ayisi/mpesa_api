<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Paybill_model
 *
 * @author epic-code
 */
class Mv_paybill_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    function getPaybillPayments($payment_id = FALSE, $start_at = 0, $limit = 10000, $voucher_not_processed = FALSE) {

        $results = array();

        if ($payment_id) {
            $this->db->where("paybill_payments_id", $payment_id);
        }

        if ($limit) {
            $this->db->limit($limit, $start_at);
        }
        if ($voucher_not_processed) {
            $this->db->where("voucher_processed", NULL);
        }

        $this->db->order_by(1, 'desc');

        $data = $this->db->get("mv_paybill_payments ppp");

        if ($data->num_rows()) {
            foreach ($data->result() as $row) {
                $results[] = $row;
            }
        }

        return $results;
    }

    function insertPaybillPayment($MSISDN, $customer_name, $agent_number, $agent_code, $agent_name, $FirstName, $MiddleName, $LastName, $TransType, $TransID, $TransTime, $TransAmount, $BusinessShortCode, $BillRefNumber, $OrgAccountBalance, $sender_ip_address){

        $saved_customerID = Null;

        $this->db->select('id');
        $this->db->where('phonenumber', $MSISDN);
        $this->db->limit(1);
        $query = $this->db->get('customers');
        if($query->num_rows() == 1){
            //if query finds one row relating to this user then execute code accordingly here
            $result = $query->result_array();
            $saved_customerID = $result[0]['id'];

            $this->db->trans_start();
            $this->db->insert("transactions", array(
                'transaction_type' => $TransType,
                'transaction_id' => $TransID,
                'transaction_time' => $TransTime,
                'transaction_amount' => $TransAmount,
                'business_shortcode' => $BusinessShortCode,
                //'bill_reference_number' => preg_replace('/\s+/', '', ucfirst(strtolower($BillRefNumber))),
                'bill_reference_number' => $BillRefNumber,
                'policy_owner' => $customer_name,
                'organisation_account_balance' => $OrgAccountBalance,
                'customer_id' => $saved_customerID,
                'sender_ip_address' => $sender_ip_address
            ));
            $transaction_customer_id = $this->db->insert_id();

            $this->db->insert("customer_transactions", array(
                'customer_id' => $saved_customerID,
                'transaction_id' => $transaction_customer_id
            ));

            $this->db->insert("receipt", array(
                'transaction_type' => $TransType,
                'transaction_id' => $TransID,
                'transaction_time' => $TransTime,
                'transaction_amount' => $TransAmount,
                'business_shortcode' => $BusinessShortCode,
                //'bill_reference_number' => preg_replace('/\s+/', '', ucfirst(strtolower($BillRefNumber))),
                'bill_reference_number' => $BillRefNumber,
                'policy_owner' => $customer_name,
                'organisation_account_balance' => $OrgAccountBalance,
                'customer_id' => $saved_customerID,
                'sender_ip_address' => $sender_ip_address,
                'agent_number' => $agent_number,
                'agent_broker_code' => $agent_code,
                'agent_name' => $agent_name,
            ));
            $this->db->trans_complete();

        } else {
            $this->db->trans_start();
            $this->db->insert("customers", array(
                'firstname' => $FirstName,
                'middlename' => $MiddleName,
                'lastname' => $LastName,
                'phonenumber' => $MSISDN,
                'email' => ""
            ));

            $this->db->insert("transactions", array(
                'transaction_type' => $TransType,
                'transaction_id' => $TransID,
                'transaction_time' => $TransTime,
                'transaction_amount' => $TransAmount,
                'business_shortcode' => $BusinessShortCode,
                //'bill_reference_number' => preg_replace('/\s+/', '', ucfirst(strtolower($BillRefNumber))),
                'bill_reference_number' => $BillRefNumber,
                'policy_owner' => $customer_name,
                'organisation_account_balance' => $OrgAccountBalance,
                'customer_id' => $this->db->insert_id(),
                'sender_ip_address' => $sender_ip_address
            ));

            $transaction_customer_id = $this->db->insert_id();

            $this->db->insert("customer_transactions", array(
                'customer_id' => $saved_customerID,
                'transaction_id' => $transaction_customer_id
            ));

            $this->db->insert("receipt", array(
                'transaction_type' => $TransType,
                'transaction_id' => $TransID,
                'transaction_time' => $TransTime,
                'transaction_amount' => $TransAmount,
                'business_shortcode' => $BusinessShortCode,
                //'bill_reference_number' => preg_replace('/\s+/', '', ucfirst(strtolower($BillRefNumber))),
                'bill_reference_number' => $BillRefNumber,
                'policy_owner' => $customer_name,
                'organisation_account_balance' => $OrgAccountBalance,
                'customer_id' => $saved_customerID,
                'sender_ip_address' => $sender_ip_address
            ));
            $this->db->trans_complete();

        }

    }

    public function number_exists($phonenumber)
    {
        $this->db->where('phonenumber', $phonenumber);
        $query = $this->db->get('customers');
        if($query->num_rows >= 1)
        {
            //if query finds one row relating to this user then execute code accordingly here
        }
        $this->db->select('*')
            ->from('customers')
            ->where(['phonenumber' => $phonenumber])
            //->or_where(['user_name'] => $user_id])
            ->limit(1);
    }

    function updatePaybillPayment($paybill_payments_id, $update_param) {
        $this->db->where('paybill_payments_id', $paybill_payments_id);
        return $this->db->update('mv_paybill_payments', $update_param);
    }

    function updateMpesaPaybillPayment($paybill_payments_id, $mobile_voucher) {

        $table_name = "mv_paybill_payments";

        $param = array(
            'voucher_processed' => 1,
            'timestamp_voucher_processed' => time(),
            'voucher_code_issued' => $mobile_voucher
        );
        $this->db->where('paybill_payments_id', $paybill_payments_id);
        return $this->db->update($table_name, $param);
    }


}

?>