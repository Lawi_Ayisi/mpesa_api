<?php
/**
 * Created by PhpStorm.
 * User: lawi
 * Date: 3/27/17
 * Time: 11:50 AM
 */
?>
<div class="container">
<h1>Pay via MPESA:</h1>
<form method="post" class="require-validation col-md-8 col-md-offset-2" action="<?php echo site_url('/requestcheckout');?>">

    <div class="form-group">
        <label for="policynumber">Policy Number:</label>
        <input type="text" name="policynumber" id="policynumber" tabindex="1" class="form-control" placeholder="Enter Policy Number" value="">
    </div>

    <div class="form-group">
        <label for="amount">Amount:</label>
        <input type="text" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Enter Amount" value="">
    </div>

    <div class="form-group">
        <label for="amount">Phonenumber:</label>
        <input type="text" name="number" id="number" tabindex="1" class="form-control" placeholder="2547111111111" value="">
    </div>

    <button type="submit" name="checkout" class="btn btn-primary">Pay Now</button>
</form>

<!--<p>NB: Since this sample uses a real paybill number it makes real transactions. Hence encouraged to test with the lowest amount 10/=</p> -->

</div>