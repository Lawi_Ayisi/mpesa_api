<?php

class Smshelper {
    public static function sendSMS($text, $recipient) {
        $client = new \infobip\api\client\SendMultipleTextualSmsAdvanced(new \infobip\api\configuration\BasicAuthConfiguration('JubileeKenya', 'Nairobi9'));

        $destination = new  \infobip\api\model\Destination();
        $destination->setTo($recipient);

        $message = new \infobip\api\model\sms\mt\send\Message();
        $message->setFrom('Jubinsure');
        $message->setDestinations([$destination]);
        $message->setText($text);

        $requestBody = new \infobip\api\model\sms\mt\send\textual\SMSAdvancedTextualRequest();
        $requestBody->setMessages([$message]);

        $response = $client->execute($requestBody);

        return $response;
    }
}