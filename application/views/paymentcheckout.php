<?php
/**
 * Created by IntelliJ IDEA.
 * User: lawi
 * Date: 6/20/17
 * Time: 1:03 PM
 * Generates the view for User/Staff to key in customer details and posts it to Requestcheckout controller
 */
?>
<div class="container">
    <h1 class="col-md-8 col-md-offset-2 paymentH1">Make Payment via MPESA:</h1>
    <div id="form">
        <form id="paymentForm" method="post" class="require-validation col-md-8 col-md-offset-2 payment-form"  data-toggle="validator" action="<?php echo site_url('/requestcheckout');?>">

            <div class="form-group">
                <label for="policynumber">Policy Number:</label>
                <input type="text" name="policynumber" id="policynumber" tabindex="1" class="form-control" placeholder="Enter Policy Number" value="" required>
            </div>

            <div class="form-group">
                <label for="lineofbusiness">Line of Business:</label>
                <select id="selectpicker" class="selectpicker" data-style="btn" name="lineofbusiness" tabindex="1" required>
                    <option>--Select From List--</option>
                    <option>Life Business</option>
                    <option>General Business</option>
                    <option>Medical Business</option>
                    <option>Pensions</option>
                </select>
            </div>

            <div class="form-group">
                <label for="amount">Amount:</label>
                <input type="text" name="amount" id="amount" tabindex="1" class="form-control" placeholder="Enter Amount" required value="">
            </div>

            <div class="form-group">
                <label for="amount">Phonenumber:</label>
                <input type="text" name="number" id="number" tabindex="1" class="form-control" placeholder="2547111111111" required value="">
            </div>

            <div class="form-group">
                <label for="captchaBox">Code</label>
                <span  id="captcha"></span>
                <input type="text" id="captchaBox" name="captcha" class="form-control captcha" maxlength="4" placeholder="Enter captcha code" required tabindex=1 />
            </div>
            <button type="submit" name="checkout" class="btn btn-pay btn-lg center-block">Pay Now</button>
        </form>
    </div>
</div>
