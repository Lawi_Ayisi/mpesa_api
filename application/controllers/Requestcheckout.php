<?php
error_reporting(0);

$this->render('requestcheckout', 'full_width');

$ENDPOINT = "https://safaricom.co.ke/mpesa_online/lnmo_checkout_server.php?wsdl";

$CALLBACK_URL = "https://10.176.0.119:443/mpesa_api/processcheckout";
$CALL_BACK_METHOD = "POST";
$PAYBILL_NO = "";
$PASSKEY = "";

//Form action implementation
$AMOUNT = $_POST['amount'];
$NUMBER = $_POST['number']; //format 254700000000
$PRODUCT_ID = $_POST['policynumber'];
$LINE_OF_BUSINESS = $_POST['lineofbusiness'];

if($LINE_OF_BUSINESS == "Life Business" ){
    $PAYBILL_NO = "328100";
    $PASSKEY = "1cc62ad03ce81de04a0f2d6c505ceac0a32c929f9ec61cbb7137e103c0b40826"; //328100
} elseif ($LINE_OF_BUSINESS == "General Business" || $LINE_OF_BUSINESS == "Medical Business"){
    $PAYBILL_NO = "328102";
    $PASSKEY = "39fe9a4a4960b19bbc714bab142f92e1f83a4fb62bcec59df89338ac6a487fde"; // 328102
} elseif ($LINE_OF_BUSINESS == "Pensions"){
    $PAYBILL_NO = "328103";
    $PASSKEY = "3cecb9f82b6f1be593a5e10ba2c57b6a0396491fdbb3cf532c738fae1b11d789"; // 328103
}


//$PAYBILL_NO = "328100";
//$PAYBILL_NO = "328102";
//$PAYBILL_NO = "328103";
//$PRODUCT_ID = "General or Medical Insurance Policy";
//$PRODUCT_ID = "Life Insurance Policy";
//$PRODUCT_ID = "Pension Policy";

$MERCHENTS_ID = $PAYBILL_NO;

$MERCHANT_TRANSACTION_ID = generateRandomString();
$INFO = $PAYBILL_NO;
//$TIMESTAMP = "20160510161908";//MUST BE THE ONE USED IN CREATING THE PASSWORD

$TIMESTAMP = date("YmdHis",time());
//$PASSKEY = "39fe9a4a4960b19bbc714bab142f92e1f83a4fb62bcec59df89338ac6a487fde"; // 328102
//$PASSKEY = "3cecb9f82b6f1be593a5e10ba2c57b6a0396491fdbb3cf532c738fae1b11d789"; // 328103
//$PASSKEY = "1cc62ad03ce81de04a0f2d6c505ceac0a32c929f9ec61cbb7137e103c0b40826"; //328100
/*NB : PASSWORD MUST BE OBTAIN FROM THE BELOW FORMAT */
 $PASSWORD = base64_encode(hash("sha256", $MERCHENTS_ID.$PASSKEY.$TIMESTAMP));

//$PASSWORD ='ZmRmZDYwYzIzZDQxZDc5ODYwMTIzYjUxNzNkZDMwMDRjNGRkZTY2ZDQ3ZTI0YjVjODc4ZTExNTNjMDA1YTcwNw==';

/*// Json implementation
$data = json_decode(file_get_contents('php://input'), true);
print_r($data);
$AMOUNT = $data["amount"];
$NUMBER = $data["number"];
$PRODUCT_ID = $data["policynumber"];*/


$body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:tns="tns:ns" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"xmlns:soapenc="http://schemas.xmlsoap.org/soap/encoding/"><soapenv:Header><tns:CheckOutHeader><MERCHANT_ID>'.$PAYBILL_NO.'</MERCHANT_ID><PASSWORD>'.$PASSWORD.'</PASSWORD><TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP></tns:CheckOutHeader></soapenv:Header><soapenv:Body><tns:processCheckOutRequest><MERCHANT_TRANSACTION_ID>'.$MERCHANT_TRANSACTION_ID.'</MERCHANT_TRANSACTION_ID><REFERENCE_ID>'.$PRODUCT_ID.'</REFERENCE_ID><AMOUNT>'.$AMOUNT.'</AMOUNT><MSISDN>'.$NUMBER.'</MSISDN><ENC_PARAMS></ENC_PARAMS><CALL_BACK_URL>'.$CALLBACK_URL.'</CALL_BACK_URL><CALL_BACK_METHOD>'.$CALL_BACK_METHOD.'</CALL_BACK_METHOD><TIMESTAMP>'.$TIMESTAMP.'</TIMESTAMP></tns:processCheckOutRequest></soapenv:Body></soapenv:Envelope>'; /// Your SOAP XML needs to be in this variable

if (!is_dir('./successful_online_checkout_request')) {
    mkdir('./successful_online_checkout_request', 0777, true);
}
$name = "./successful_online_checkout_request/log-" . time() . ".xml";
$f = fopen("$name", "w");
fwrite($f, $body);
fclose($f);

try{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $ENDPOINT);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    curl_setopt($ch, CURLOPT_VERBOSE, '0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, '0');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, '0');

    $output = curl_exec($ch);
    curl_close($ch);


// Check if any error occured
if(curl_errno($ch))
{
    echo 'Error no : '.curl_errno($ch).' Curl error: ' . curl_error($ch);
}

//print_r("To complete this transaction, enter your Bonga PIN on your handset. if you don't have one dial *126*5# for instructions ");

    if (!is_dir('./successful_online_checkout_response')) {
        mkdir('./successful_online_checkout_response', 0777, true);
    }
    $name = "./successful_online_checkout_response/log-" . time() . ".xml";
    $f = fopen("$name", "w");
    fwrite($f, $output);
    fclose($f);

//now process the checkout;
include ('processcheckout.php');
processcheckout($MERCHANT_TRANSACTION_ID, $ENDPOINT,$PASSWORD,$TIMESTAMP,$PAYBILL_NO);
}

catch(Exception $ex){
echo $ex;
}

function generateRandomString() {
    $length = 10;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}



?>